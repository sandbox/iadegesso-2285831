CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation

INTRODUCTION
------------

Passaparola is a module to allow single citizens to recommend other citizens as good and reliable providers of services. Authorization from this person is granted outside the box (that's sociotechnical folks!), this is just a community-private board where to refer members of the same community to other people good in what they do (also community outsiders). It has been conceived for informal services (like babysitting, errand running, housework doing, home schooling and the like) but people can of course use it also to advice their peers about good places and honest professionals. Passaparola is "word of mouth" in Italian.

Conceived within the Condiviviamo Project developed at the University of Milano-Bicocca and supervised by Federico Cabitza, PhD.

 For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/iadegesso/2285831

REQUIREMENTS
------------

This module requires the following modules:
 * Geolocation (https://www.drupal.org/project/geolocation)
 * Fivestar (https://www.drupal.org/project/fivestar)
 * Voting API (https://www.drupal.org/project/votingapi)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


 * You may want to disable Toolbar module, since its output clashes with
   Administration Menu.

 *[IMPORTANT] When enable the Passaparole module, remember to also active the “Geolocation Google Maps” for the fully support of geolocation. It allow to add the address for the research by geolocation.
