<?php

//pagina dei fornitori che si auto-compila
function fornitore_abc_edit($abc_id) {
    $id_f = $abc_id->id;
    ////Estraggo i dati generici del fornitore
    $query = db_select('fornitori_db', 'f')
            ->condition('f.id', $id_f, '=')
            ->fields('f', array('nome_fornitore',
                'mail',
                'tel',
                'indirizzo',
                'category',))
            ->execute();
    $rows = array();
    foreach ($query as $result) {
        $rows[] = array(
            check_plain($result->nome_fornitore),
            check_plain($result->mail),
            check_plain($result->tel),
            check_plain($result->indirizzo),
            check_plain($result->category),
        );
    }

    //mi posiziono nell'array
    $rows = $rows[0];
    //salvo tutte le variabili per creare i contenuti statici della pagina
    $nome_f = $rows[0];
    $mail_f = $rows[1];
    $tel_f = $rows[2];
    $indirizzo_f = $rows[3];
    $category_f = $rows[4];

    //Estraggo la data di fruizioni, raccomandi?, valutazione, commenti
    $query2 = db_select('fruizioni', 'f')
            ->condition('f.IDFornitore', $id_f, '=')
            ->fields('f', array('IDUtente',
                'raccomandi',
                'valutazione',
                'commento',))
            ->execute();
    $v_media = 0;
    $valutazione_tot = 0;
    $cont = 0;
    $user_name = '';
    $raccomandazioni_positive = 0;
    $raccomandazioni_negative = 0;
    $date = '';
    $comments = '';
    //$geolocation = 'Click<a href=""> here</a> if you wanna search any providers neaby '; //PARTE AGGIUNTA
    $rows2 = array();
    //ciclo in cui popolo le variabili
    foreach ($query2 as $result) {
        //valutazione media
        $valutazione_tot = $valutazione_tot + check_plain($result->valutazione);
        $cont = $cont + 1;
        //raccomandazioni positive-negative
        $raccomandi = check_plain($result->raccomandi);
        $si_no = '';
        if ($raccomandi == 1) {
            $raccomandazioni_positive++;
            $si_no = 'YES';
        } else {
            $raccomandazioni_negative++;
            $si_no = 'NO';
        }

        //commenti
        $name = id_to_user(check_plain($result->IDUtente));
        //drupal_set_message(print_r($name, true));
        $commento = check_plain($result->commento);
        $comments = $comments . '<h3><b> ' . $name . '</b></h3> Recommend : ' . $si_no . '.<br>' . 'Comment: "' . $commento . '"<br><br>';
    }

    if ($cont == 0)
        $v_media = $valutazione_tot;
    else
        $v_media = number_format($valutazione_tot / $cont, 2, ',', '.');

    //popolo l'array delle keyword
    $keyword = '';
    //estraggo le kewword riferite a questo fornitore
    $query3 = db_select('f_k', 'f')
            ->condition('f.IDFornitore', $id_f, '=')
            ->fields('f', array('nomeKeyword'))
            ->execute();
    foreach ($query3 as $result) {
        $keyword = $keyword . check_plain($result->nomeKeyword) . ', ';
    }
    //elimino l'ultima virgola
    $keyword = substr($keyword, 0, strlen($keyword) - 2);

    //creo il bottone che rimanda alla pagina di modifica fornitore
    $modify = '';
    //lo visualizzo solo se l'utente loggato è quello ad aver creato quell'inserzione
    $idf = $id_f;
    $query = db_select('fornitori_db', 'n')
            ->fields('n', array('id_creator'))
            ->condition('n.id', $idf, '=')
            ->execute();
    foreach ($query as $result) {
        $id_creator = check_plain($result->id_creator);
    }
    global $user;
    $userid = $user->uid;
    if ($userid == $id_creator) {
        $modify = '<p align="center">' . l('Edit Supplier information', 'edit_fornitore/' . $idf) . '</p>';
    }

    return print_r(('<b>Name of Company/Person: </b>' . $nome_f . '<br>' .
            '<b>Category: </b>' . get_category_name($category_f) . '<br>' .
            '<b>Keyword: </b>' . $keyword . '<br>' .
            '<br>' .
            '<b>CONTACT: </b><br>' .
            '<b>E-Mail: </b>' . $mail_f . '<br>' .
            '<b>Telephone: </b>' . $tel_f . '<br>' .
            '<b>Address: </b>' . $indirizzo_f . '<br><br>' .
            '<b>Media of rating: </b>' . $v_media . '<br>' .
            '<b>Positive recommendation: </b>' . $raccomandazioni_positive . '<br>' .
            '<b>Negative recommendation: </b>' . $raccomandazioni_negative . '<br><br>' .
            '<b>COMMENTS: </b><br>' . $comments) /*. '<em>' . $geolocation . '</em><strong>' . $name . '<strong><br/><br/>' . $modify*/, true);
}

function id_to_user($id) {
    $query_user = db_select('users', 'u')
            ->condition('u.uid', $id, '=')
            ->fields('u', array('name'))
            ->execute();
    $user = '';
    foreach ($query_user as $u) {
        $user = check_plain($u->name);
    }
    return $user;
}
