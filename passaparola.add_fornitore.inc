<?php
    //FORM AGGIUNGI FORNITORE
    
    function add_fornitore_form($form, &$form_state) {

    $form['description'] = array(
        '#type' => 'item',
        '#title' => t('Enter the data of your service provider'),
    );


    $form['nome_fornitore'] = array(
        '#type' => 'textfield',
        '#title' => t('Add Company or supplier name'),
        '#description' => "Insert name of service provider (Azienda o persona che sia)",
        '#size' => 30,
        '#maxlength' => 30,
        '#required' => TRUE,
    );

    ///////////////////////////////////////////////
    $form['recapito'] = array(
       '#type' => 'fieldset',
       '#title' => t('Contacts'),
       '#required' => TRUE,
       '#collapsible' => TRUE,
       '#collapsed' => FALSE,
     );

    $form['recapito']['mail'] = array(
        '#type' => 'textfield',
        '#title' => t('Mail address'),
        //'#required' => TRUE,
        '#size' => 20,
        '#maxlength' => 50,
        //'#default_value' => 'nomail',
    );
    $form['recapito']['tel'] = array(
        '#type' => 'textfield',
        '#title' => t('Telephone'),
        //'#required' => TRUE,
        //'#default_value' => '0',
    );
    $form['indirizzo'] = array(
        '#type' => 'textfield',
         '#title' => t('Address'),
        //'#required' => TRUE,
    );
    ////////////////////////////////////////////////////
    //QUI ANDR� LA SCELTA DELLA CATEGORIA 
  $form['status'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="status">',
    '#suffix' => '</div>',
    '#markup' => '',
  );
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //funzioni che estraggono dal db gli array per il menu
  $top_category_array = create_top_category_array();
  $second_category_array = create_second_category_array($top_category_array);
  //categoria di primo livello
  $form['top_category_list'] = array(
      '#type' => 'select',
      '#title' => t('Choose the Service Category'),
      '#options' => $top_category_array,
  );

  //devo creare l'array fatto per le options del menu
  foreach ($second_category_array as $categoria => $sottoclassi) { //le sottoclassi hanno come indici gli id univoci nel db
    $n = array_search($categoria, $top_category_array);
    //drupal_set_message(print_r($categoria, true));
    $option_array = array();
    foreach ($sottoclassi as $key => $value) {
        $option_array[$key] = ($value[0]);

    }

    //drupal_set_message(print_r($option_array, true));
    //immetto i form totali ciclando le top_category
    $form[$n] = array(
        '#type' => 'select',
        '#states' => array(
            'visible' => array(
                ':input[name="top_category_list"]' => array('value' => $n),
            ),
        ),
        '#options' => $option_array,
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    

    $form['keyword'] = array(
        '#type' => 'textfield',
        '#title' => t('Add Keyword'),
        '#required' => TRUE,
        '#description' => "Add Keywords, separated by commas",
        '#size' => 150,
    );  
      
    $form['date'] = array(
        '#type' => 'date',
        '#title' => t('Service fruition date'),
        '#required' => TRUE,

    );

    //QUI LA VALUTAZIONE A STELLINE
    $form['valutazione'] = array(
        '#type' => 'fivestar',
        '#stars' => 5,
        '#title' => t('Rating'),
        '#required' => TRUE,
    );
    
    //a capo
    $form['s'] = array(
        '#type' => 'markup',
        '#prefix' => '<br>',
      );

    $active = array(0 => t('No'), 1 => t('Yes'));
    $form['raccomandi'] = array(
        '#type' => 'radios',
        '#title' => t('Recommend?'),
        '#default_value' => isset($node->active) ? $node->active : 1,
        '#options' => $active,
        '#required' => TRUE,
    );

    //textarea
    $form['commento'] = array(
        '#title' => t('Comment'),
        '#type' => 'textarea',
        '#description' => t('Insert your comment'),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
        '#validate' => array('add_validate'),
        '#submit' => array('add_fornitore_submit'),
    );
    return $form;
    }

    //VALIDATION FORM
    function add_validate($form, &$form_state) {

        //valido nome (se non � mai stato inserito)
        $nome = $form_state['values']['nome_fornitore'];
        $categoria = $form_state['values']['top_category_list'];
        $categoria2 = $form_state['values'][$categoria];
        //$categoria2 = get_category_id($categoria, $form_state);
        //controllo se c'� gi� un fornitore con lo stesso nome e la stessa categoria
        $inserito = db_select('fornitori_db','n')
            ->fields('n',array('nome_fornitore'))
            ->condition('n.nome_fornitore',$nome,'=')
            ->condition('n.category', $categoria2, '=')
            ->execute();
        $insert = array('x');
    
        foreach($inserito as $result) {
            $insert[0] = array(
            check_plain($result -> nome_fornitore),
            );
        }
        //$insert = $insert[0];
        if($insert[0] != 'x'){
            form_set_error('name', t('Service provider already exists'));
        }
        //drupal_set_message(print_r($insert, true));
        //valida anno
        $year = $form_state['values']['date']['year'];
        $month = $form_state['values']['date']['month'];
        $day = $form_state['values']['date']['day'];
        $current_year = date('Y');
        //array('year' => 2007, 'month' => 2, 'day'=> 15);
        if ($year && ($year < 1980 || $year > $current_year)) {
        form_set_error('year', t('Enter a realistic year.'));
        }

        //o mail o recapito obbligatori
        $mail_V = $form_state['values']['mail'] ;
        $tel_V = $form_state['values']['tel'];
        //drupal_set_message(print_r(array($mail_V, $tel_V), true));
        ////
        if ($mail_V == '' && $tel_V == ''){
            form_set_error('recapito',t('Add Email or Telephone number'));
        }

        //verifica num telefono
         $value = $tel_V;
         if ($value !== '' && (!is_numeric($value)  || $value <= 0)) { //|| intval($value) != $value
            form_set_error('tel_V', t('Enter a real Telephone Number'));
        }

        //valida categoria obbligatoria
        //$categoria_validate = $form_state['values']['top_category_list'];
        //if($categoria_validate == 0) form_set_error('categoria', t('You need to choose the category'));
        
        //devo verificare che chi sta inserendo fornitore sia loggato
        
        global $user;
        $userid=$user->uid;
        //drupal_set_message($userid, true);
        if(!$userid) form_set_error('tel_V', t('You have to be logged'));
        
        }

    //SUBMIT FORM
    function add_fornitore_submit($form, &$form_state) {
        $nome = $form_state['values']['nome_fornitore'];
        $mail = $form_state['values']['mail'];
        $tel = $form_state['values']['tel'];
        $indirizzo = $form_state['values']['indirizzo'];
        $date = (
                    $form_state['values']['date']['day'] . '/' .
                    $form_state['values']['date']['month'] . '/' .
                    $form_state['values']['date']['year']
                      );
        $valutazione = $form_state['values']['valutazione'];
        $raccomandi = $form_state['values']['raccomandi'];
        $commento = $form_state['values']['commento'];
        $categoria = $form_state['values']['top_category_list'];
        $categoria2 = $form_state['values'][$categoria];
        //$categoria2 = get_category_id($categoria, $form_state);
        ////
        global $user;
        $userid=$user->uid;
        
        $inserimentoDB1 = db_insert('fornitori_db')
            ->fields(array(
                        'id_creator' => $userid,
                        'nome_fornitore'=> $nome,
                        'mail'=> $mail,
                        'tel'=> $tel,
                        'indirizzo'=> $indirizzo,
                        'category'=> $categoria2,
                    ))
            -> execute();
            
        //estraggo l'id del fornitore che ho appena inserito
        $id_fornitore = db_select('fornitori_db','n')
            ->fields('n',array('id'))
            ->condition('n.nome_fornitore',$nome,'=')
            ->condition('n.category', $categoria2, '=')
            ->execute();
        $id_for = array();
    
        foreach($id_fornitore as $result) {
            $id_for[] = array(
            check_plain($result -> id),
            );
        }
        $id_for = $id_for[0];
        $id_for = $id_for[0];  
        
            
        $inserimentoDB4 = db_insert('fruizioni')
            ->fields(array(
                        'IDFornitore' => $id_for,
                        'IDUtente' => $userid,
                        'date'=> $date,
                        'valutazione'=> $valutazione / 20,
                        'raccomandi'=> $raccomandi,
                        'commento'=> $commento,
                    ))
            -> execute();
                        

       //gestisco le keyword
        $keyword = $form_state['values']['keyword'];
        $keyword = trim($keyword);               
        $keyword = strtolower($keyword);
        $keyword = str_replace(" ", "", $keyword);
        $key_array= explode(',', $keyword);
        //inserisco le keyword
        for($i = 0; $i<sizeof($key_array); $i++){
            $element = $key_array[$i];
            //verifico se esiste gi� la keyword
            $exist = verify_keyowrd($element);
            if($exist){
                insert_f_k($element, $id_for);
            }else{
                insert_f_k($element, $id_for);
                insert_keyword_db($element);
            }
        }              
        //drupal_set_message(print_r($key_array, true));
        drupal_set_message(t('The form has been submitted.'));
    }

    
    function verify_keyowrd($k){
      $query = db_select('keyword_db','n')
            ->fields('n',array('nome_k'))
            ->condition('n.nome_k',$k,'=')
            ->execute();
        $rows = array();
        foreach($query as $result) {
             $rows[] = array(
             check_plain($result -> nome_k),);
        }
        //controllo che ci sia almeno una keyword che corrisponde
        if($rows){
          return TRUE;
        } else return FALSE;     
    }
    
    function insert_f_k($keyword, $idf){
        $insert = db_insert('f_k')
            ->fields(array(
                        'IDFornitore' => $idf,
                        'nomeKeyword' => $keyword,
                    ))
            -> execute();
    }
    
    function insert_keyword_db($keyword){
        $insert = db_insert('keyword_db')
            ->fields(array(
                        'nome_k' => $keyword,
                    ))
            -> execute();
    }
    
    