<?php

/*
 *
 */

function search_fornitore_form($form, &$form_state) {

    $scelta = array(3 => t('Search by name'), 1 => t('Search by category'), 2 => t('Search by last modified'), 4 => t('Search by geolocation'));


    $form['scelta_ricerca'] = array(
        '#type' => 'radios',
        '#title' => t('Search service provider or type of service'),
        //'#default_value' => isset($node->active) ? $node->active : 0,
        '#options' => $scelta,
            //'#description' => t('Search page'),
    );
    //ricerca per nome
    $form['by_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Search service provider by name'),
        '#states' => array(
            'visible' => array(
                ':input[name="scelta_ricerca"]' => array('value' => 3),
            ),
        ),
        '#size' => 100,
    );

    //ricerca per geolocalizzazione
    $form['by_geoloc'] = array(
        '#type' => 'radios',
        '#title' => t('Search service provider by geolocation'),
        '#options' => array(
            t('Actual location'),
            t('Default location - kept by your profile'),
        ),
        '#states' => array(
            'visible' => array(
                ':input[name="scelta_ricerca"]' => array('value' => 4),
            ),
        ),
    );

    $form['by_geoloc_dist'] = array(
        '#type' => 'select',
        '#title' => t('Max distance (km)'),
        '#options' => array(
            '5', '10', '15', '20', '25', '30',
        ),
        '#states' => array(
            'visible' => array(
                ':input[name="scelta_ricerca"]' => array('value' => 4),
            ),
        ),
    );

    $form['hidden_lat'] = array('#type' => 'hidden', '#id' => 'hidden_lat', '#default_value' => "");
    $form['hidden_lng'] = array('#type' => 'hidden', '#id' => 'hidden_lng', '#default_value' => "");

    drupal_add_js('

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                alert("Geolocation is not supported by this browser.");
            }
        }
        function showPosition(position) {
            document.getElementsByName("hidden_lat")[0].value = position.coords.latitude;
            document.getElementsByName("hidden_lng")[0].value = position.coords.longitude;  
        }
        document.getElementsByName("by_geoloc")[0].addEventListener("click",getLocation);

        var radioGeoloc = document.getElementsByName("by_geoloc");
        for (var i = 0; i < radioGeoloc.length; i++) {
            radioGeoloc[i].required = false;
        }

        var radioButtons = [];

        radioButtons[0] = document.getElementById("edit-scelta-ricerca-1");
        radioButtons[1] = document.getElementById("edit-scelta-ricerca-2");
        radioButtons[2] = document.getElementById("edit-scelta-ricerca-3");

        function unsetRequired() {
            for (var i = 0; i < radioGeoloc.length; i++) {
                radioGeoloc[i].required = false;
            }
        }

        for (var i = 0; i < radioButtons.length; i++) {
            radioButtons[i].addEventListener("click", unsetRequired);
        }

        function setRequired() {
            for (var i = 0; i < radioGeoloc.length; i++) {
                radioGeoloc[i].required = true;
            }
        }

        var radio4 = document.getElementById("edit-scelta-ricerca-4");
        radio4.addEventListener("click", setRequired);

     ', array('type' => 'inline', 'scope' => 'footer', 'weight' => 5)
    );

    //ricerca per categoria
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    //funzioni che estraggono dal db gli array per il menu
    $top_category_array = create_top_category_array();
    $second_category_array = create_second_category_array($top_category_array);
    //categoria di primo livello
    $form['top_category_list'] = array(
        '#type' => 'select',
        '#title' => t('Choose the Service Category'),
        '#options' => $top_category_array,
        '#states' => array(
            'visible' => array(
                ':input[name="scelta_ricerca"]' => array('value' => 1),
            ),
        ),
    );

    //devo creare l'array fatto per le options del menu
    foreach ($second_category_array as $categoria => $sottoclassi) { //le sottoclassi hanno come indici gli id univoci nel db
        $n = array_search($categoria, $top_category_array);
        //drupal_set_message(print_r($categoria, true));
        $option_array = array();
        foreach ($sottoclassi as $key => $value) {
            $option_array[$key] = ($value[0]);
        }
        //drupal_set_message(print_r($option_array, true));
        //immetto i form totali ciclando le top_category
        $form[$n] = array(
            '#type' => 'select',
            '#states' => array(
                'visible' => array(
                    ':input[name="scelta_ricerca"]' => array('value' => '1'),
                    ':input[name="top_category_list"]' => array('value' => $n),
                ),
            ),
            '#options' => $option_array,
        );
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //ricerca ultimi inseriti


    $form['search_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Search'),
        '#submit' => array('search_submit'),
    );

    return $form;
}

//SUBMIT FORM 
function search_submit($form, &$form_state) {
    $text_search = $form_state['values']['by_name'];
    $categoria = $form_state['values']['top_category_list'];
    $category2 = $form_state['values'][$categoria];
    $location = $form_state['values']['by_geoloc'];  //PARTE AGGIUNTA
    $hidden_lat = $form_state['values']['hidden_lat'];
    $hidden_lng = $form_state['values']['hidden_lng'];
    $max_distance = $form_state['values']['by_geoloc_dist'];

    //$category2 = get_category_id($categoria, $form_state);
    $scelta = $form_state['values']['scelta_ricerca'];

    global $user;
    $userid = $user->uid;
    $result = db_select('field_data_indirizzo', 'f')
            ->fields('f', array('entity_id'))
            ->condition('f.entity_id', $userid, '=')
            ->execute()
            ->fetchAll();
    $count = count($result);
    //1 = per categoria
    //2 = per ultimi inseriti
    //3 = per nome
    //4 per geoloc
    if ($scelta == 3) {//per nome
        $exist = validate_name($text_search);
        if ($text_search == '') {
            drupal_set_message('Write a name', 'error');
        } else {
            if ($exist) {
                drupal_goto('search_name_result/' . $text_search);
            } else {
                drupal_set_message('Name not found, Try another name', 'error');
            }
        }
    } else if ($scelta == 1) {//per categoria
        $exist2 = validate_category($category2);
        if ($exist2) {
            drupal_goto('search_category_result/' . $category2);
        } else
            drupal_set_message('Empty Category, try another ', 'error');
    }else if ($scelta == 2) {//per ultimi inseriti
        drupal_goto('search_last');
    } else if ($scelta == 4) {
        $adjusted_max_distance = ($max_distance + 1) * 5;
        if ($location == 0) {
            drupal_goto('search_provider_by_actual_location/' . $hidden_lat . "/" . $hidden_lng . "/" . $adjusted_max_distance);
        } else if ($location == 1) {
            if ($count == 0) {
                drupal_set_message('Default location not set in your account', 'error');
                drupal_goto('search_fornitore');
            } else {
                drupal_goto('search_provider_by_default_location/' . $adjusted_max_distance);
            }
        }
    } else {
        drupal_set_message('Choose a type of search and try again', 'error');
    }
}

function validate_name($name) {
    $query = db_select('fornitori_db', 'n')
            ->fields('n', array('nome_fornitore'))
            ->condition('n.nome_fornitore', '%' . $name . '%', 'LIKE')
            ->execute();
    $rows = array();
    foreach ($query as $result) {
        $rows[] = array(
            check_plain($result->nome_fornitore),);
    }
    //controllo che ci sia almeno un nome che corrisponde
    if ($rows) {
        return TRUE;
    } else
        return FALSE;
    //drupal_set_message(print_r($id_name, true));
}

function validate_category($category) {
    $query = db_select('fornitori_db', 'n')
            ->fields('n', array('category'))
            ->condition('n.category', $category, '=')
            ->execute();
    $rows = array();
    foreach ($query as $result) {
        $rows[] = array(
            check_plain($result->category),);
    }
    //controllo che ci sia almeno un nome che corrisponde
    if ($rows) {
        return TRUE;
    } else
        return FALSE;
}
