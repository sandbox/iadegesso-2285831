<?php
	function edit_rating_form($form, &$form_state, $abc_rf) {

		$idf = $abc_rf -> id ; //id del fornitore
		global $user;
		$userid = $user->uid; //id dell'utente corrente
		//estraggo anche il nome del fornitore
	    $query2 = db_select('fornitori_db', 'f')
	        ->condition('f.id',$idf,'=')
	        ->fields('f', array('nome_fornitore', 'id_creator'))
	        ->execute();

	    foreach($query2 as $r){
	    	$nome_f = check_plain($r -> nome_fornitore);
	    	$id_creatore = check_plain($r -> id_creator);
	    }
		//drupal_set_message(print_r($userid, true));
	    $query3 = db_select('fruizioni', 'f')
	        ->condition('f.IDFornitore',$idf,'=')
	        ->fields('f', array('IDUtente' ))
	        ->execute();

	    $utenti_votanti = array();
	    foreach($query3 as $v){
	    	$utenti_votanti[] = array(check_plain($v -> IDUtente));
	    }
	    //drupal_set_message(print_r($utenti_votanti, true));
	    //$utenti_votanti = $utenti_votanti[0];
	    $exist = False;
	    for ($i=0; $i < sizeof($utenti_votanti); $i++){
	    	if($utenti_votanti[$i][0] == $userid){
	    		//drupal_set_message(print_r($utenti_votanti[$i][0], true));
	    		$id_creator = $utenti_votanti[$i][0];
	    		$exist = True;

	    		break;
	    	}
	    }
	    
	    
		if ($exist){
			$query = db_select('fruizioni', 'f')
		        ->condition('f.IDFornitore',$idf,'=')
		        ->condition('f.IDUtente',$userid,'=')
		        ->fields('f', array('raccomandi',
		                            'valutazione',
		                            'commento',))
		        ->execute();
			//salvo le variabili della prima query
		   
			foreach($query as $result) {
	        	$raccomandi = check_plain($result -> raccomandi);
	            $valutazione = check_plain($result -> valutazione);
	            $commento = check_plain($result -> commento);
	        }
		    $valutazione = $valutazione * 20;
		
	        //drupal_set_message(print_r($valutazione, true));
	        //devo controllare che esista un rating precednte per quell'utente
		

	    $form['nome_fornitore'] = array(
        '#type' => 'textfield',
        '#title' => t('Company or supplier name'),
        '#size' => 30,
        '#maxlength' => 30,
        '#default_value' => $nome_f,
        '#attributes' => array('readonly' => 'readonly'),
    	);
    	//variabili da passare al submit (invisibili all'utente)
		$form['id_f'] = array(
	        '#type' => 'textfield',
	        '#default_value' => $idf,
	        '#access' => FALSE,
	        '#attributes' => array('readonly' => 'readonly'),
	    );
	    $form['id_creator'] = array(
	        '#type' => 'textfield',
	        '#default_value' => $id_creator,
	        '#access' => FALSE,
	        '#attributes' => array('readonly' => 'readonly'),
	    );

    	$active = array(0 => t('No'), 1 => t('Yes'));
	    $form['raccomandi'] = array(
	        '#type' => 'radios',
	        '#title' => t('Recommend?'),
	        '#default_value' => isset($node->active) ? $node->active : 1,
	        '#options' => $active,
	        '#required' => TRUE,
	        '#default_value' => $raccomandi,
	    );

	    //QUI LA VALUTAZIONE A STELLINE
	    $form['valutazione'] = array(
	        '#type' => 'fivestar',
	        '#stars' => 5,
	        '#title' => t('Change Rating'),
	        '#required' => TRUE,
	        '#default_value' => $valutazione,
	    );

	    $form['commento'] = array(
	        '#title' => t('Edit your Comment'),
	        '#type' => 'textarea',
	        '#description' => t('Insert your comment'),
	        '#default_value' => $commento,
	    );

	    $form['submit'] = array(
	        '#type' => 'submit',
	        '#value' => 'Submit',
	        //'#validate' => array('edit_validate'),
	        '#submit' => array('edit_fornitore_submit'),
	    );

	    if ($id_creatore != $userid){
	    $form['delete_rating'] = array(
	        '#type' => 'submit',
	        '#value' => 'Delete Rating',
	        //'#validate' => array('edit_validate'),
	        '#submit' => array('delete_rating_submit'),
	    );
	}
	    return $form;
			    
	    }
	    else
	    	form_set_error('user', t('You are not autorized to edit this rate'));
	}

	//submit form
	function edit_fornitore_submit($form, &$form_state) {
    	$nome = $form_state['values']['nome_fornitore'];
            //drupal_set_message(print_r($nome, true));
        $idf = $form_state['values']['id_f'];
            //drupal_set_message(print_r($idf, true));
        $valutazione = $form_state['values']['valutazione'];
        $raccomandi = $form_state['values']['raccomandi'];
        $commento = $form_state['values']['commento'];

		//modifico i valori nella tabella fruizioni
		$user_creator =  $form_state['values']['id_creator'];
		db_update('fruizioni')
			->fields(array(
				'raccomandi' => $raccomandi,
				'valutazione' => $valutazione / 20,
				'commento'=> $commento,
				))
			->condition('IDFornitore', $idf)
			->condition('IDUtente', $user_creator)
			->execute();

        //drupal_set_message(print_r($key_array, true));
        drupal_set_message(t('The form has been submitted.'));
    }

function delete_rating_submit($form, &$form_state) {
	  $idf = $form_state['values']['id_f'];
	  $user_creator =  $form_state['values']['id_creator'];
	  //cancello il rating di quella persona per quel fornitore
	  db_delete('fruizioni')
	   ->condition('IDFornitore', $idf)
	   ->condition('IDUtente', $user_creator)
	   ->execute();
	  drupal_goto('search_fornitore');
}

	