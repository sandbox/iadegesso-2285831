<?php
	function edit_fornitore_form($form, &$form_state, $abc_rf) {

		$idf = $abc_rf -> id ;      
		//estraggo tutte le informazioni correnti del fornitore avente tale id
		$query = db_select('fornitori_db','n')
            ->fields('n',array('id', 'id_creator','nome_fornitore', 'mail', 'tel', 'indirizzo', 'category' ))
            ->condition('n.id',$idf,'=')
            ->execute();
        foreach($query as $result) {
        	$id_creator = check_plain($result -> id_creator);
            $nome_f = check_plain($result -> nome_fornitore);
            $mail = check_plain($result -> mail);
            $tel = check_plain($result -> tel);
            $indirizzo = check_plain($result -> indirizzo);
            $category = check_plain($result -> category);
        }  

        //echo $nome_f;

    	$form['description'] = array(
        '#type' => 'item',
        '#title' => t('Edit the service provider'),
    	);


    $form['nome_fornitore'] = array(
        '#type' => 'textfield',
        '#title' => t('Company or supplier name'),
        //'#description' => "Insert name of service provider (Azienda o persona che sia)",
        '#size' => 30,
        '#maxlength' => 30,
        '#default_value' => $nome_f,
        '#attributes' => array('readonly' => 'readonly'),
    );

    $form['id_f'] = array(
        '#type' => 'textfield',
        '#default_value' => $idf,
        '#access' => FALSE,
        '#attributes' => array('readonly' => 'readonly'),
    );
    $form['id_creator'] = array(
        '#type' => 'textfield',
        '#default_value' => $id_creator,
        '#access' => FALSE,
        '#attributes' => array('readonly' => 'readonly'),
    );

    ///////////////////////////////////////////////
    $form['recapito'] = array(
       '#type' => 'fieldset',
       '#title' => t('Contacts'),
       '#required' => TRUE,
       '#collapsible' => TRUE,
       '#collapsed' => FALSE,
     );

    $form['recapito']['mail'] = array(
        '#type' => 'textfield',
        '#title' => t('Edit Mail address'),
        //'#required' => TRUE,
        '#size' => 20,
        '#maxlength' => 50,
        '#default_value' => $mail,
    );
    $form['recapito']['tel'] = array(
        '#type' => 'textfield',
        '#title' => t('Edit Telephone number'),
        //'#required' => TRUE,
        '#default_value' => $tel,
    );
    $form['indirizzo'] = array(
        '#type' => 'textfield',
        '#title' => t('Change Address'),
        //'#required' => TRUE,
        '#default_value' => $indirizzo,
    );
    ////////////////////////////////////////////////////
    //QUI ANDRà LA SCELTA DELLA CATEGORIA 
  $form['status'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="status">',
    '#suffix' => '</div>',
    '#markup' => '',
  );
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //funzioni che estraggono dal db gli array per il menu
  $top_category_array = create_top_category_array();
  $second_category_array = create_second_category_array($top_category_array);  

  $query = db_select('second_category_fornitori','n')
            ->fields('n',array('nome_top_category' ))
            ->condition('n.id2',$category,'=')
            ->execute();
        foreach($query as $result) {
        	$nome_top_category = check_plain($result -> nome_top_category);
        }  
        foreach ($top_category_array as $key => $value) {
		  	if($nome_top_category == $value)
		  	{
		  		$top_category = $key;
		  		break;
		  	}
		  }

  //categoria di primo livello
  $form['top_category_list'] = array(
      '#type' => 'select',
      '#title' => t('Choose the Service Category'),
      '#options' => $top_category_array,
      '#default_value' => $top_category,
  );

  //devo creare l'array fatto per le options del menu
  foreach ($second_category_array as $categoria => $sottoclassi) { //le sottoclassi hanno come indici gli id univoci nel db
    $n = array_search($categoria, $top_category_array);
    //drupal_set_message(print_r($categoria, true));
    $option_array = array();
    foreach ($sottoclassi as $key => $value) {
        $option_array[$key] = ($value[0]);

    }

    //drupal_set_message(print_r($option_array, true));
    //immetto i form totali ciclando le top_category
    $form[$n] = array(
        '#type' => 'select',
        '#states' => array(
            'visible' => array(
                ':input[name="top_category_list"]' => array('value' => $n),
            ),
        ),
        '#options' => $option_array,
        '#default_value' => $category,
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $query3 = db_select('fruizioni','n')
            ->fields('n',array('date','raccomandi', 'valutazione', 'commento' ))
            ->condition('n.IDFornitore',$idf,'=')
            ->execute();
        foreach($query3 as $result) {
        	$date = check_plain($result -> date);
            $raccomandi = check_plain($result -> raccomandi);
            $valutazione = check_plain($result -> valutazione);
            $commento = check_plain($result -> commento);
        }  
        $valutazione = $valutazione * 20;

    $form['keyword'] = array(
        '#type' => 'textfield',
        '#title' => t('Add new Keyword'),
        //'#required' => TRUE,
        '#description' => "Add another Keywords, separated by commas",
        '#size' => 150,
    );      
    //devo formattare la data per il default value      
      $str_date = explode("/", $date);     
      $str_date['month'] = $str_date[1];
      $str_date['day'] = $str_date[0];
      $str_date['year'] = $str_date[2];

    $form['date'] = array(
        '#type' => 'date',
        '#title' => t('Change Service fruition date'),
        '#required' => TRUE,
        '#default_value' => $str_date,
    );

    //QUI LA VALUTAZIONE A STELLINE
    $form['valutazione'] = array(
        '#type' => 'fivestar',
        '#stars' => 5,
        '#title' => t('Change Rating'),
        '#required' => TRUE,
        '#default_value' => $valutazione,
    );
    
    //a capo
    $form['s'] = array(
        '#type' => 'markup',
        '#prefix' => '<br>',
      );

    $active = array(0 => t('No'), 1 => t('Yes'));
    $form['raccomandi'] = array(
        '#type' => 'radios',
        '#title' => t('Recommend?'),
        '#default_value' => isset($node->active) ? $node->active : 1,
        '#options' => $active,
        '#required' => TRUE,
        '#default_value' => $raccomandi,
    );

    //textarea
    $form['commento'] = array(
        '#title' => t('Edit your Comment'),
        '#type' => 'textarea',
        '#description' => t('Insert your comment'),
        '#default_value' => $commento,
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
        '#validate' => array('edit_validate'),
        '#submit' => array('edit_fornitore_submit'),
    );
    return $form;

	}	

//VALIDATION FORM
    function edit_validate($form, &$form_state) {
	
        //valido nome (se non è mai stato inserito)
        $nome = $form_state['values']['nome_fornitore'];
            //drupal_set_message(print_r($nome, true));
        $idf = $form_state['values']['id_f'];
            //drupal_set_message(print_r($idf, true));
        $categoria = $form_state['values']['top_category_list'];
        $categoria2 = $form_state['values'][$categoria];

        //valida anno
        $date = $form_state['values']['date'];
        //drupal_set_message(print_r($date, true));
        $year = $form_state['values']['date']['year'];
        $month = $form_state['values']['date']['month'];
        $day = $form_state['values']['date']['day'];
        $current_year = date('Y');
        //array('year' => 2007, 'month' => 2, 'day'=> 15);
        if ($year && ($year < 1980 || $year > $current_year)) {
        form_set_error('year', t('Enter a realistic year.'));
        }

        //o mail o recapito obbligatori
        $mail_V = $form_state['values']['mail'] ;
        $tel_V = $form_state['values']['tel'];
        //drupal_set_message(print_r(array($mail_V, $tel_V), true));
        ////
        if ($mail_V == '' && $tel_V == ''){
            form_set_error('recapito',t('Add Email or Telephone number'));
        }

        //verifica num telefono
         $value = $tel_V;
         if ($value !== '' && (!is_numeric($value)  || $value <= 0)) { //|| intval($value) != $value
            form_set_error('tel_V', t('Enter a real Telephone Number'));
        }

        //valida categoria obbligatoria
        //$categoria_validate = $form_state['values']['top_category_list'];
        //if($categoria_validate == 0) form_set_error('categoria', t('You need to choose the category'));
        
        //devo verificare che chi sta inserendo fornitore sia loggato
        
        global $user;
        $userid=$user->uid;
        $user_creator =  $form_state['values']['id_creator'];
        //drupal_set_message($userid, true);
        if(!$userid)
        	form_set_error('user', t('You have to be logged'));
        elseif($userid != $user_creator){
        	form_set_error('user', t('You must be the creator to edit this insertion'));
        } 
        
        }

    //SUBMIT FORM
     
    function edit_fornitore_submit($form, &$form_state) {
    	$nome = $form_state['values']['nome_fornitore'];
            //drupal_set_message(print_r($nome, true));
        $idf = $form_state['values']['id_f'];
            //drupal_set_message(print_r($idf, true));
        $mail = $form_state['values']['mail'];
        $tel = $form_state['values']['tel'];
        $indirizzo = $form_state['values']['indirizzo'];
        $date = (
                    $form_state['values']['date']['day'] . '/' .
                    $form_state['values']['date']['month'] . '/' .
                    $form_state['values']['date']['year']
                      );
        $valutazione = $form_state['values']['valutazione'];
        $raccomandi = $form_state['values']['raccomandi'];
        $commento = $form_state['values']['commento'];
        $categoria = $form_state['values']['top_category_list'];
        $categoria2 = $form_state['values'][$categoria];
        //$categoria2 = get_category_id($categoria, $form_state);
        ////
        global $user;
        $userid=$user->uid;

        db_update('fornitori_db')
			->fields(array(
				'mail' => $mail,
				'tel' => $tel,
				'indirizzo' => $indirizzo,
				'category'=> $categoria2,
				))
			->condition('id', $idf)
			->execute();

		//modifico i valori nella tabella fruizioni
		$user_creator =  $form_state['values']['id_creator'];
		db_update('fruizioni')
			->fields(array(
				'date' => $date,
				'raccomandi' => $raccomandi,
				'valutazione' => $valutazione / 20,
				'commento'=> $commento,
				))
			->condition('IDFornitore', $idf)
			->condition('IDUtente', $user_creator)
			->execute();

       //gestisco le keyword
        $keyword = $form_state['values']['keyword'];
        $keyword = trim($keyword);               
        $keyword = strtolower($keyword);
        $keyword = str_replace(" ", "", $keyword);
        $key_array= explode(',', $keyword);
        //inserisco le keyword
        for($i = 0; $i<sizeof($key_array); $i++){
            $element = $key_array[$i];
            //verifico se esiste già la keyword
            $exist = verify_keyowrd($element);
            if($exist){
            	$exist_f_k = verify_f_k($element,$idf);
            	if(!$exist_f_k){
                insert_f_k($element, $idf);}
        		}	
	        else{
	                insert_f_k($element, $idf);
	                insert_keyword_db($element);
	            }
        }              
        //drupal_set_message(print_r($key_array, true));
        drupal_set_message(t('The form has been submitted.'));
    }


    function verify_keyowrd($k){
      $query = db_select('keyword_db','n')
            ->fields('n',array('nome_k'))
            ->condition('n.nome_k',$k,'=')
            ->execute();
        $rows = array();
        foreach($query as $result) {
             $rows[] = array(
             check_plain($result -> nome_k),);
        }
        //controllo che ci sia almeno una keyword che corrisponde
        if($rows){
          return TRUE;
        } else return FALSE;     
    }

    function verify_f_k($k, $idf){
      $query = db_select('f_k','n')
            ->fields('n',array('nomeKeyword'))
            ->condition('n.nomeKeyword',$k,'=')
			->condition('n.IDFornitore',$idf,'=')
            ->execute();
        $rows = array();
        foreach($query as $result) {
             $rows[] = array(
             check_plain($result -> nomeKeyword),);
        }
        //controllo che ci sia almeno una f_k uguale a questa
        if($rows){
          return TRUE;
        } else return FALSE;     
    }
    
    function insert_f_k($keyword, $idf){
        $insert = db_insert('f_k')
            ->fields(array(
                        'IDFornitore' => $idf,
                        'nomeKeyword' => $keyword,
                    ))
            -> execute();
    }
    
    function insert_keyword_db($keyword){
        $insert = db_insert('keyword_db')
            ->fields(array(
                        'nome_k' => $keyword,
                    ))
            -> execute();
    }
    
    