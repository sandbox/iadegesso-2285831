<?php
function upload_form($form_state) {
  $form = array();

  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Upload Category'),
    '#description' => t("Upload a JSON category file"),
    //'#upload_location'    => "public://category/",
    //"#upload_validators"  => array("file_validate_extensions" => array("json")),
    //'#required' => TRUE,

  );
  $form['submit_upload'] = array(
    '#type'  =>  'submit', 
    '#value'  =>  'Upload',
    '#validate' => array('upload_validate'),
    '#submit' => array('upload_submit'),
  );
  return $form;
} 

function upload_validate($form, &$form_state) {
  $file = file_save_upload('file', array(
    
    // Validates file is really an image.
    //'file_validate_is_image' => array(),
    
    // Validate extensions.
    'file_validate_extensions' => array('json'),
  ));
  // If the file passed validation:
  if ($file) {
    // Move the file into the Drupal file system.
    if ($file = file_move($file, 'public://')) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t("Failed to write the uploaded file to the site's file folder."));
    }
  }
  else {
    form_set_error('file', t('No file was uploaded.'));
  }
}


function upload_submit($form, &$form_state) {
  //popolo il database con le categorie merceologiche
 // $x = file_validate_extensions($form_state['values']['file_upload'], array("json"));
  $file = $form_state['storage']['file'];
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);
  $uri = $file->uri;
  $json_path = drupal_realpath($uri); //path del file json da parsare

  $string = file_get_contents($json_path);
  $json_a = drupal_json_decode($string, true);
  //svuoto le tabelle precedentemente create
  //db_delete('top_category_fornitori') ->execute();
  //CANCELLO TUTTE LE TABELLE DEL DATABASE (DA FIXARE IN FUTURO)
  db_delete('second_category_fornitori') ->execute();
  db_delete('fornitori_db') ->execute();
  db_delete('keyword_db') ->execute();
  db_delete('f_k') ->execute();
  db_delete('fruizioni') ->execute();
  //drupal_set_message(print_r($json_a, true));
  foreach ($json_a as $sottocategory => $topcategory) {
    /*
    if(is_array($topcategory)) {
        //drupal_set_message( "$key:\n");
    } else {
        drupal_set_message("$key => $val\n");
        
    }*/
    //drupal_set_message("$key => $val\n");
    
    
    //inserisco il topcategory nella tabella top_category_fornitori
    /*
    $inserimentoDBf = db_insert('top_category_fornitori')
            ->fields(array(
                        'nome_top_category'=> $topcategory,
                    ))
            -> execute();*/
    //inserisco nella seconda tabella entrambi
    $inserimentoDBf2 = db_insert('second_category_fornitori')
            ->fields(array(
                        'nome_second_category'=> $sottocategory,
                        'nome_top_category'=> $topcategory,
                    ))
            -> execute();      
  }

  //drupal_set_message(print_r($json_path, true));
  //
  //

   drupal_set_message(t('You have add new category tree'));
}