<?php

    function search_name($abc_name){
        $name_URL = $abc_name;
        $id_fornitori = search_id_by_name($name_URL);
        return format_page_search_output($id_fornitori);
    }

    function search_id_by_name($name){
        $query = db_select('fornitori_db','n')
            ->fields('n',array('id','nome_fornitore'))
            ->condition('n.nome_fornitore','%'.$name.'%','LIKE')
            ->execute();
        $rows = array();
        foreach($query as $result) {
            $rows[] = array(
            check_plain($result -> id),
            );
        }
        return $rows;
    }