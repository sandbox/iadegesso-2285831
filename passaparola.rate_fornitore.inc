<?php
    function rate_fornitore_form($form, &$form_state, $abc_rf) {
        
    $idf = $abc_rf -> id ;        
    $name= name_by_id($idf);
    
    
    $fornitore = array($idf => t($name));
    $form['id_fornitore'] = array(
        '#type' => 'radios',
        '#title' => t('Name of service provider:'),
        '#default_value' => isset($node->active) ? $node->active : $idf,
        '#options' => $fornitore,
        '#required' => TRUE,
        );

    $active = array(0 => t('No'), 1 => t('Yes'));
    $form['raccomandi'] = array(
        '#type' => 'radios',
        '#title' => t('Recommend?'),
        '#default_value' => isset($node->active) ? $node->active : 1,
        '#options' => $active,
        '#required' => TRUE,
    );
    
    //QUI LA VALUTAZIONE A STELLINE
    $form['valutazione'] = array(
        '#type' => 'fivestar',
        '#stars' => 5,
        '#title' => t('Rating'),
        '#default' => 3,
        '#required' => TRUE,
    );

    //textarea
    $form['commento'] = array(
        '#title' => t('Comment'),
        '#type' => 'textarea',
        '#description' => t('Insert your comment'),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
        '#validate' => array('id_validate'),
        '#submit' => array('rate_fornitore_submit'),
    );
    
    return $form;
    }
    
    //VALIDATE FORM
    function id_validate($form, &$form_state) {
        //controllo che lo stesso utente non abbia gi� commentato il fornitore

        $idf = $form_state['values']['id_fornitore'];
        global $user;
        $userid=$user->uid; //id utente corrente
        if(!$userid){ form_set_error('tel_V', t('You have to be logged'));}
        else{
        $inserito = db_select('fruizioni','n')
            ->fields('n',array('IDFornitore', 'IDUtente'))
            ->condition('n.IDFornitore',$idf,'=') //il fornitore che sto votando    
            ->condition('n.IDUtente', $userid, '=') //se c'� gi� l'id utente con cui sto votando
            ->execute();
        $insert = array('x');
    
        foreach($inserito as $result) {
            $insert[0] = array(
            check_plain($result -> IDFornitore),
            );
        }
        if($insert[0] != 'x'){
            form_set_error('name', t('You have alrady rated this supplier'));
        }
        }
    }
    //SUBIMIT FORM
    function rate_fornitore_submit($form, &$form_state) {
        $idf = $form_state['values']['id_fornitore'];
        $valutazione = $form_state['values']['valutazione'];
        $raccomandi = $form_state['values']['raccomandi'];
        $commento = $form_state['values']['commento'];
        global $user;
        $userid=$user->uid;
        $today = getdate();
        $year = $today['year'];
        $month = $today['mon'];
        $day = $today['mday'];
        $date = $day . '/' . $month . '/' . $year;

        $inserimentoDB4 = db_insert('fruizioni')
            ->fields(array(
                        'IDFornitore' => $idf,
                        'IDUtente' => $userid,
                        'valutazione'=> $valutazione / 20,
                        'raccomandi'=> $raccomandi,
                        'date' => $date,
                        'commento'=> $commento,
                    ))
            -> execute();
                        
        drupal_set_message(t('The form has been submitted.'));
    }

function name_by_id($id){
        $query = db_select('fornitori_db','n')
            ->fields('n',array('id','nome_fornitore'))
            ->condition('n.id',$id,'=')
            ->execute();
        foreach($query as $result) {
            $nome_f = check_plain($result -> nome_fornitore);
        }
        return $nome_f;
    }
    