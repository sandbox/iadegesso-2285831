<?php

    function last_insert(){
        $id_fornitori = search_last();
        //return format_page_search_outputz($id_fornitori);
        return format_page_search_output($id_fornitori);
    }
    
    function search_last(){
        $caunter = 0;
        $query = db_select('fornitori_db','n')
            ->fields('n',array('id'))
            ->orderBy('id', 'DESC')
            ->range (0, 20)
            ->execute()
			->fetchAll();
			$caunter = count($query);			
        $rows = array();
        foreach($query as $result) {
    
            $rows[] = array(
            check_plain($result -> id),
            );
        }
        if ($caunter == 0){
            drupal_set_message('No providers found', 'error');
            drupal_goto('search_fornitore');
        }
        return $rows;
    }