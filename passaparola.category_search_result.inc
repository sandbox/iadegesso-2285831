<?php

    function search_category($abc_category){
        $category = $abc_category -> category ;
        $id_fornitori = search_id_by_category($category);
        return format_page_search_output($id_fornitori);
    }

    function search_id_by_category($category){
        $query = db_select('fornitori_db','n')
            ->fields('n',array('id','category'))
            ->condition('n.category',$category,'=')
            ->execute();
        $rows = array();
        foreach($query as $result) {
            $rows[] = array(
            check_plain($result -> id),
            );
        }
        //drupal_set_message(print_r($rows, true));
        return $rows;
    }